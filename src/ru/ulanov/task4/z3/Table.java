package ru.ulanov.task4.z3;

public class Table {
    public static void main(String[] args) {
        for (int g = 1; g < 10; g++) {
            for (int v = 1; v < 10; v++) {
                System.out.print(g * v + "  ");
            }
            System.out.println();
        }
    }
}
