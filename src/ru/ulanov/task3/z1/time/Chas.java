package ru.ulanov.task3.z1.time;
import java.util.Scanner;
public class Chas {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите сколько секунд хотите конвертировать в часы: ");
        int s = in.nextInt();
        in.close();
        int c = s/3600;
        System.out.println(s+" секунд составялет "+ c+ " час(а/ов)");
    }
}
