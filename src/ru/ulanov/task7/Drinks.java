package ru.ulanov.task7;

public enum Drinks {
    TEA(1, "Чай", 50),
    COFFEE(2, "Кофе", 60),
    HOTCHOCO(3, "Горячий шоколад", 70),
    CAPPUCCINO(4, "Капучино", 50),
    LATTE(5, "Латте", 60),
    CHOCO(6, "Шоколад", 50),
    ESPRESSO(7, "Эспрессо", 30),
    DOUBLECHOCO(8, "Двойной шоколад", 50),
    WATER(9, "Вода", 10),
    CACAO(10, "Какао", 30);

    int number;
    String name;
    int cost;

    Drinks(int number, String name, int cost) {
        this.number = number;
        this.name = name;
        this.cost = cost;
    }

}
