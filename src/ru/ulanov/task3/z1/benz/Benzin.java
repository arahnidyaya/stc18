package ru.ulanov.task3.z1.benz;
import java.util.Scanner;
public class Benzin {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество литров бензина: ");
        int c = in.nextInt();
        in.close();
        int b = 44;
        int s = c * b;
        System.out.println("Стоимость " + b + " литров равна " + s);
    }
}
