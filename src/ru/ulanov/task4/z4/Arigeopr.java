package ru.ulanov.task4.z4;

import java.util.Scanner;

public class Arigeopr {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите 1 для отображения арифметической прогрессии, 2 для отображения геометрической прогрессии, 3 для отображения обеих прогрессий: ");
        int q = inn.nextInt();
        if (q > 3 || q < 1) {
            System.out.println("Неверный выбор прогрессии ");
            return;
        }
        System.out.println("Введите количество чисел в прогрессии: ");
        int n = inn.nextInt();
        System.out.println("Введите первое число прогрессии: ");
        int b = inn.nextInt();
        System.out.println("Введите шаг прогрессии: ");
        int d = inn.nextInt();
        inn.close();
        if (q == 1 || q == 3) {
            System.out.println("Арифметическая прогрессия: ");
            for (int a = 1; a < n + 1; a++) {
                System.out.print(+(b + d * (a - 1)) + "  ");
            }
        }
        if (q == 2 || q == 3) {
            System.out.println(" ");
            System.out.println("Геометрическая прогрессия: ");
            for (int a = 1; a < n + 1; a++) {
                System.out.print(+(int) (b * Math.pow(d, a - 1)) + "  ");
            }
        }
    }

}
