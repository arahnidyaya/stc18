package ru.ulanov.task7;

public class VmImpl implements Vm {
    int money = 0;

    @Override
    public String getMenu() {
        String result = " ";
        for (Drinks d : Drinks.values()) {
            result += (d.number + " " + d.name + " " + d.cost + " рублей \n");
        }
        return result;
    }

    @Override
    public void putMoney(int money) {
        this.money += money;

    }

    @Override
    public Drinks getDrinks(int number) {
        Drinks[] drinks = Drinks.values();
        if (drinks[number] == null) {
            System.out.println("такого напитка нет");
            return null;
        }
        if (drinks[number].cost > money) {
            System.out.println("Недостаточно средств");
            return null;
        }
        money = -drinks[number].cost;
        return drinks[number];
    }
}
