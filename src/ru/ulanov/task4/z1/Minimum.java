package ru.ulanov.task4.z1;

import java.util.Scanner;

public class Minimum {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        int c1 = inn.nextInt();
        Scanner inn2 = new Scanner(System.in);
        System.out.println("Введите второе число: ");
        int c2 = inn2.nextInt();
        inn2.close();
        int min = Math.min(c1,c2);
        System.out.println("Минимальное число " + min);
//        if (c1 > c2 && c1 != c2) {
//            System.out.println("Минимальное число " + c2);
//        }
//        if (c1 < c2 && c1 != c2) {
//            System.out.println("Минимальное число " + c1);
//        }
//        if (c1 == c2) {
//            System.out.println("Числа равны ");
//        }
    }
}
