package ru.ulanov.task7;

public interface Vm {
    String getMenu();

    void putMoney(int money);

    Drinks getDrinks(int number);
}




