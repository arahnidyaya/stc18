package ru.ulanov.task4.z2;

import java.util.Scanner;

public class Opi {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите число: ");
        int a = inn.nextInt();
        inn.close();
        if (a > 0 && a != 0) {
            System.out.println("Введенное число положительное");
        }
        if (a < 0 && a != 0) {
            System.out.println("Введенное число отрицательное");
        }
        if (a % 2 == 0 && a != 0) {
            System.out.println("Введенное число четное");
        } else {
            if (a != 0) {
                System.out.println("Введенное число не четное");
            }
        }
        if (a == 0) {
            System.out.println("Введенное число равно нулю");
        }
    }
}
